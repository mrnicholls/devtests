﻿namespace ClassLibrary1

module Say =
    #if INTERACTIVE
    #r @"C:\Users\m_r_n\source\repos\DevTestsSolution\DevTests\bin\Debug\DevTests.exe"
    #endif
    open DevTests

    let x = Dict<int,int>()
     
    x.Add(5,5)
    x.Add(2,2)
    x.Add(1,1)
    x.Add(3,3)
    x.Add(7,7)
    x.Add(6,6)
    x.Add(4,4)
    let y = x.AsEnumerable() |> Seq.toList



    let hello name =
        printfn "Hello %s" name

    type List<'a> = 
    | Empty
    | Item of ('a * List<'a>)

    let rec map : ('a -> 'b) -> List<'a> -> List<'b> = 
        fun f ->
            function 
            | Empty         -> Empty
            | Item (a,next) -> Item (f a,map f next)

    let rec map2 : ('a -> 'b) -> List<'a> -> List<'b> = 
        let rec mapAcc : ('a -> 'b) -> List<'a> -> List<'b> -> List<'b> = 
            fun f xs acc ->
                match xs with
                | Empty -> acc
                | Item (a,next) -> mapAcc f xs (Item (f a,acc))
        fun f xs ->
            mapAcc f xs Empty
            
    let xs = Item (1,Item(2,(Item(3,Empty))))
    let ys = map (fun x -> x + 1) xs 
            
module BTree = 

    type Tree<'t> =
    | Empty
    | Node of Node<'t>
    and Node<'t> = { data : 't; left : Tree<'t>; right : Tree<'t> }

    let rec fold<'a,'t> : ('a -> 'a) -> ('a -> Node<'t> -> 'a) -> 'a -> Tree<'t> -> 'a = 
        fun fEmpty fNode acc ->
            function 
            | Empty     -> fEmpty acc
            | Node n    -> 
                let foldRight = n.right |> fold fEmpty fNode acc
                let foldThis = fNode foldRight n
                let foldLeft = n.left |> fold fEmpty fNode foldThis
                foldLeft

    let mkNode a b c = Node { data = a; left = c; right = b}

    let t = 
        (mkNode 4 
            (mkNode 2 
                (mkNode 1 Empty Empty)
                (mkNode 3 Empty Empty))
            (mkNode 8
                (mkNode 6 Empty Empty)
                (mkNode 10 Empty Empty)))
    
    fold (fun x -> x) (fun x n -> x + n.data) 0 t
    fold (fun x -> x) (fun x n -> seq { yield! x; yield n.data}) Seq.empty t


    // see https://gist.github.com/friedbrice/1a6d6ee21d22f82a1aec18792f9189ad for scala version
    let rec fold2<'a,'t> : ('a -> 'a) -> ('a -> Node<'t> -> 'a) -> 'a -> Tree<'t> -> 'a =         
        fun fEmpty fNode acc node ->
            let rec helper : List<Tree<'t>> -> 'a -> 'a = 
                fun rest acc2 -> 
                    match rest with
                    | []            -> acc2
                    | Empty :: ls   -> helper ls (fEmpty acc2)
                    | Node n :: ls  -> helper (n.left :: n.right :: ls) (fNode acc2 n)                                                       
            helper [node] acc
                
    fold2 (fun x -> x) (fun x n -> seq { yield! x; yield n.data}) Seq.empty t |> Seq.toList


    //https://stackoverflow.com/questions/48727302/build-list-from-binary-tree-inorder-traversal-using-function-fold

    //def fold[B](init: B)(op: (B, A) => B): B = {
    //    def go(stack: List[(A, Tree[A])], current: Tree[A], acc: B): B = (current, stack) match {
    //        case (Empty, Nil) => acc
    //        case (Empty, (d, r) :: xs) => go(xs, r, op(acc, d))
    //        case (Node(l, d, r), _) => go((d, r) +: stack, l, acc)
    //    }

    //    go(Nil, this, init)
    //}
    //https://stackoverflow.com/questions/48727302/build-list-from-binary-tree-inorder-traversal-using-function-fold
    let rec fold3<'a,'t> : ('a -> 'a) -> ('a -> Node<'t> -> 'a) -> 'a -> Tree<'t> -> 'a =         
        fun fEmpty fNode acc node ->
        // we recurse through the tree, if we find something on the left
        // then we move to it, and save it for later in the stack.
        // we only ever process a node IF we have no 
            let rec loop : Tree<'t> -> List<Node<'t>> -> 'a -> 'a = 
                fun current stack acc2 -> 
                    match (current,stack) with
                    | (Empty,[])        -> fEmpty acc2
                    | (Empty, n :: ls)  -> loop (n.right) ls (fNode acc2 n)
                    | (Node n,_)        -> loop (n.left) (n :: stack) acc2
            loop node [] acc
                
    fold3 (fun x -> x) (fun x n -> seq { yield! x; yield n.data}) Seq.empty t |> Seq.toList

    let rec TraverseInOrder<'a,'t> : Tree<'t> -> seq<Node<'t>> =         
        fun node ->
        // we recurse through the tree, if we find something on the left
        // then we move to it, and save it for later in the stack.
        // we only ever process a node IF we have no 
            let rec loop : Tree<'t> -> List<Node<'t>> -> seq<Node<'t>> -> seq<Node<'t>> = 
                fun current stack acc2 -> 
                    match (current,stack) with
                    | (Empty,[])        -> acc2
                    | (Empty, n :: ls)  -> loop (n.right) ls (seq { yield n; yield! acc2; })
                    | (Node n,_)        -> loop (n.left) (n :: stack) acc2
            loop node [] Seq.empty

    TraverseInOrder t |> Seq.toList |> List.map (fun d -> d.data)
    //     2
    //  1      4
    //       3   5

// 2  []
// 1  [2]
// () [2]
// 

module Tree = 

    type Tree<'LeafData,'INodeData> =
        | LeafNode of 'LeafData
        | InternalNode of 'INodeData * Tree<'LeafData,'INodeData> seq    

    let rec cata fLeaf fNode (tree:Tree<'LeafData,'INodeData>) :'r = 
        let recurse = cata fLeaf fNode  
        match tree with
        | LeafNode leafInfo -> 
            fLeaf leafInfo 
        | InternalNode (nodeInfo,subtrees) -> 
            fNode nodeInfo (subtrees |> Seq.map recurse)

    let rec fold fLeaf fNode acc (tree:Tree<'LeafData,'INodeData>) :'r = 
        let recurse = fold fLeaf fNode  
        match tree with
        | LeafNode leafInfo -> 
            fLeaf acc leafInfo 
        | InternalNode (nodeInfo,subtrees) -> 
            // determine the local accumulator at this level
            let localAccum = fNode acc nodeInfo
            // thread the local accumulator through all the subitems using Seq.fold
            let finalAccum = subtrees |> Seq.fold recurse localAccum 
            // ... and return it
            finalAccum 
            