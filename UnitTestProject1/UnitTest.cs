﻿using System;
using DevTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;

namespace UnitTestProject
{
    public static class Helper
    {
        public static bool Compare<T>(IEnumerable<T> xs, IEnumerable<T> ys)
        {
            if (xs.Count() == ys.Count())
            {
                return xs.Zip(ys, (x, y) => EqualityComparer<T>.Default.Equals(x, y)).All(x => x);
            }
            else
            {
                return false;
            }
        }
    }

    //[TestClass]
    //public class ListItemTests
    //{
    //    [TestMethod]
    //    public void MapBind()
    //    {
    //        var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
    //        var ys = new SingleList<int>(xs);
    //        var zs =
    //            from y in ys.head
    //            from z in new SingleList<int>.ListItem(new SingleList<int>.ListItem(SingleList<int>.Empty.Value, y), y)
    //            select z * 2;
    //        var answer = new List<int>(new int[] { 4, 4, 2, 2, 4, 4, 4, 4, 6, 6, 4, 4, 8, 8, 4, 4, 4, 4 });
    //        Assert.IsTrue(Helper.Compare(answer, SingleList<int>.AListItemUnion.AsEnumerable(zs)));
    //    }
    //    [TestMethod]
    //    public void Map()
    //    {
    //        var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
    //        var ys = new SingleList<int>(xs);
    //        var zs =
    //            from y in ys.head
    //            select y * 2;
    //        var answer = new List<int>(new int[] { 4, 2, 4, 4, 6, 4, 8, 4, 4 });
    //        Assert.IsTrue(Helper.Compare(answer, SingleList<int>.AListItemUnion.AsEnumerable(zs)));
    //    }
    //}

    [TestClass]
    public class SingleListTest
    {
        [TestMethod]
        public void InsertException()
        {
            SingleList<int> xs = new DevTests.SingleList<int>();
            try
            {
                xs.Insert(2, 0);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
            }
        }

        [TestMethod]
        public void AddAndIndexGetter()
        {
            SingleList<int> xs = new DevTests.SingleList<int>();
            xs.Add(1);
            xs.Add(2);
            Assert.AreEqual(xs[1], 2);
            Assert.AreEqual(xs[0], 1);
        }
        [TestMethod]
        public void Add()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4 });
            var ys = new SingleList<int>(xs);
            ys.Add(5);
            xs.Add(5);
            Assert.IsTrue(Helper.Compare(xs, ys.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveSimple()
        {
            var xs = new List<int>(new int[] { 1, 2, 2, 3, 2, 4 });
            var ys = new SingleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { 1, 2, 3, 2, 4 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(isRemoved);
            Assert.AreEqual(ys.Count, 5);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveComplex()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { 1, 2, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(isRemoved);
            Assert.AreEqual(ys.Count, 8);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveEmpty()
        {
            var xs = new List<int>(new int[] { });
            var ys = new SingleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(!isRemoved);
            Assert.AreEqual(ys.Count, 0);
        }
        [TestMethod]
        public void RemoveNonExistent()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            var isRemoved = ys.Remove(0);
            var answer = new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(!isRemoved);
            Assert.AreEqual(ys.Count, 9);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveAt()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            ys.RemoveAt(2);
            var answer = new int[] { 2, 1, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 8);
        }
        [TestMethod]
        public void InsertAt()
        {
            var xs = new List<int>(new int[] { 1,2,3,4,5 });
            var ys = new SingleList<int>(xs);
            ys.Insert(2,10);
            var answer = new int[] { 1,2,10,3,4,5 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 6);
        }
        [TestMethod]
        public void RemoveAtStart()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 1 });
            var ys = new SingleList<int>(xs);
            ys.RemoveAt(0);
            var answer = new int[] { 1, 2, 2, 3, 2, 4, 2, 1 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 8);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveAtEnd()
        {
            var xs = new List<int>(new int[] { 1,2,3 });
            var ys = new SingleList<int>(xs);
            ys.RemoveAt(2);
            var answer = new int[] { 1,2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 2);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertAtEnd()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new SingleList<int>(xs);
            ys.Insert(5, 6);
            var answer = new int[] { 1, 2, 3, 4, 5, 6 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 6);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertAtStart()
        {
            var xs = new List<int>(new int[] { 2, 3, 4, 5, 6 });
            var ys = new SingleList<int>(xs);
            ys.Insert(0, 1);
            var answer = new int[] { 1, 2, 3, 4, 5, 6 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 6);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertEmpty()
        {
            var xs = new List<int>(new int[] { });
            var ys = new SingleList<int>(xs);
            ys.Insert(0, 1);
            var answer = new int[] { 1 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 1);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void IndexSetter()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            ys[2] = 3;
            var answer = new int[] { 2, 1, 3, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 9);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void IndexOf()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            Assert.AreEqual(ys.IndexOf(4), 6);
            Assert.AreEqual(ys.IndexOf(5), -1);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void Clear()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            ys.Clear();
            Assert.AreEqual(ys.Count, 0);
            Assert.AreEqual(ys.head, SingleList<int>.Empty.Value);
            Assert.IsTrue(ys.IsValid());
        }

        [TestMethod]
        public void MapBind()
        {
            var xs = new List<int>(new int[] { 3, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new SingleList<int>(xs);
            var zs =
                from y in ys
                from z in new SingleList<int>(new[] { y,y })
                select z * 2;
            var answer = new List<int>(new int[] { 6, 6, 2, 2, 4, 4, 4, 4, 6, 6, 4, 4, 8, 8, 4, 4, 4, 4 });
            Assert.IsTrue(Helper.Compare(answer, zs.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void Map()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new SingleList<int>(xs);
            var zs =
                from y in ys
                select y * 2;
            var answer = new List<int>(new int[] { 2, 4, 6, 8, 10 });
            Assert.IsTrue(Helper.Compare(answer, zs.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void Where()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new SingleList<int>(xs);
            var zs =
                from y in ys
                where y != 3
                select y * 2;
            var answer = new List<int>(new int[] { 2, 4, 8, 10 });
            Assert.IsTrue(Helper.Compare(answer, zs.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }

    }

    [TestClass]
    public class DoubleListTest
    {
        [TestMethod]
        public void AddAndIndexGetter()
        {
            DoubleList<int> xs = new DevTests.DoubleList<int>();
            xs.Add(1);
            xs.Add(2);
            Assert.AreEqual(xs[1], 2);
            Assert.AreEqual(xs[0], 1);
        }
        [TestMethod]
        public void Add()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4 });
            var ys = new DoubleList<int>(xs);
            ys.Add(5);
            xs.Add(5);
            Assert.IsTrue(Helper.Compare(xs, ys.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveSimple()
        {
            var xs = new List<int>(new int[] { 1, 2, 2, 3, 2, 4 });
            var ys = new DoubleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { 1, 2, 3, 2, 4 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(isRemoved);
            Assert.AreEqual(ys.Count, 5);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveComplex()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { 1, 2, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(isRemoved);
            Assert.AreEqual(ys.Count, 8);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveEmpty()
        {
            var xs = new List<int>(new int[] { });
            var ys = new DoubleList<int>(xs);
            var isRemoved = ys.Remove(2);
            var answer = new int[] { };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(!isRemoved);
            Assert.AreEqual(ys.Count, 0);
        }
        [TestMethod]
        public void RemoveNonExistent()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            var isRemoved = ys.Remove(0);
            var answer = new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.IsTrue(!isRemoved);
            Assert.AreEqual(ys.Count, 9);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveAt()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            ys.RemoveAt(2);
            var answer = new int[] { 2, 1, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 8);
        }
        [TestMethod]
        public void InsertAt()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new DoubleList<int>(xs);
            ys.Insert(2, 10);
            var answer = new int[] { 1, 2, 10, 3, 4, 5 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 6);
        }
        [TestMethod]
        public void RemoveAtStart()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 1 });
            var ys = new DoubleList<int>(xs);
            ys.RemoveAt(0);
            var answer = new int[] { 1, 2, 2, 3, 2, 4, 2, 1 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 8);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void RemoveAtEnd()
        {
            var xs = new List<int>(new int[] { 1, 2, 3 });
            var ys = new DoubleList<int>(xs);
            ys.RemoveAt(2);
            var answer = new int[] { 1, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 2);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertAtEnd()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new DoubleList<int>(xs);
            ys.Insert(5, 6);
            var answer = new int[] { 1, 2, 3, 4, 5, 6 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 6);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertAtStart()
        {
            var xs = new List<int>(new int[] { 2 });
            var ys = new DoubleList<int>(xs);
            ys.Insert(0, 1);
            var answer = new int[] { 1, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 2);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void InsertEmpty()
        {
            var xs = new List<int>(new int[] { });
            var ys = new DoubleList<int>(xs);
            ys.Insert(0, 1);
            var answer = new int[] { 1 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 1);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void IndexSetter()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            ys[2] = 3;
            var answer = new int[] { 2, 1, 3, 2, 3, 2, 4, 2, 2 };
            Assert.IsTrue(Helper.Compare(answer, ys.AsEnumerable()));
            Assert.AreEqual(ys.Count, 9);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void IndexOf()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            Assert.AreEqual(ys.IndexOf(4), 6);
            Assert.AreEqual(ys.IndexOf(5), -1);
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void Clear()
        {
            var xs = new List<int>(new int[] { 2, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            ys.Clear();
            Assert.AreEqual(ys.Count, 0);
            Assert.AreEqual(ys.head, DoubleList<int>.Empty.Value);
            Assert.IsTrue(ys.IsValid());
        }

        [TestMethod]
        public void MapBind()
        {
            var xs = new List<int>(new int[] { 3, 1, 2, 2, 3, 2, 4, 2, 2 });
            var ys = new DoubleList<int>(xs);
            var zs =
                from y in ys
                from z in new DoubleList<int>(new[] { y, y })
                select z * 2;
            var answer = new List<int>(new int[] { 6, 6, 2, 2, 4, 4, 4, 4, 6, 6, 4, 4, 8, 8, 4, 4, 4, 4 });
            Assert.IsTrue(Helper.Compare(answer, zs.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }
        [TestMethod]
        public void Map()
        {
            var xs = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            var ys = new DoubleList<int>(xs);
            var zs =
                from y in ys
                select y * 2;
            var answer = new List<int>(new int[] { 2, 4, 6, 8, 10 });
            Assert.IsTrue(Helper.Compare(answer, zs.AsEnumerable()));
            Assert.IsTrue(ys.IsValid());
        }

    }

    [TestClass]
    public class DictTest
    {
        [TestMethod]
        public void AddAndIndexGetter()
        {
            var xs = new Dict<int,int>();
            xs.Add(1,1);
            xs.Add(2,2);
            Assert.AreEqual(xs[1], 1);
            Assert.AreEqual(xs[2], 2);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void Add()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs,ys) =  data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
              {
                  var (dic1, dic2) = a;
                  dic1.Add(x + 1, x);
                  dic2.Add(x + 1, x);
                  return (dic1, dic2);
              });

            Assert.IsTrue(Helper.Compare(xs.Keys, ys.Keys));
            Assert.IsTrue(Helper.Compare(xs.Values, ys.Values));
            Assert.IsTrue(xs.IsValid());
        }

        [TestMethod]
        public void RemoveSimple()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });

            var r1 = xs.Remove(2);
            var r2 = ys.Remove(2);

            Assert.IsTrue(Helper.Compare(xs.Keys, ys.Keys));
            Assert.IsTrue(Helper.Compare(xs.Values, ys.Values));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }

        [TestMethod]
        public void RemoveRoot()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });

            var r1 = xs.Remove(xs.RootValue().Value);
            var r2 = ys.Remove(xs.RootValue().Value);

            Assert.AreEqual(r1, r2);
            Assert.IsTrue(Helper.Compare(xs.Keys, ys.Keys));
            Assert.IsTrue(Helper.Compare(xs.Values, ys.Values));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }

        [TestMethod]
        public void RemoveEmpty()
        {
            var xs = new Dict<int, int>();
            var ys = new Dictionary<int, int>();
            var r1 = xs.Remove(1);
            var r2 = ys.Remove(1);

            Assert.AreEqual(r1, r2);
            Assert.IsTrue(Helper.Compare(xs.Keys, ys.Keys));
            Assert.IsTrue(Helper.Compare(xs.Values, ys.Values));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void Clear()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            xs.Clear();
            ys.Clear();

            Assert.IsTrue(Helper.Compare(xs.Keys, ys.Keys));
            Assert.IsTrue(Helper.Compare(xs.Values, ys.Values));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void ContainsKey1()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.ContainsKey(2);
            var r2 = ys.ContainsKey(2);

            Assert.AreEqual(r1, true);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void ContainsKey2()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.ContainsKey(-2);
            var r2 = ys.ContainsKey(-2);

            Assert.AreEqual(r1, false);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void ContainsValue1()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.ContainsValue(2);
            var r2 = ys.ContainsValue(2);

            Assert.AreEqual(r1, true);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void ContainsValue2()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.ContainsValue(-2);
            var r2 = ys.ContainsValue(-2);

            Assert.AreEqual(r1, false);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }

        [TestMethod]
        public void ContainsTryGet1()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.TryGetValue(3);

            Assert.AreEqual(r1.HasValue, true);
            Assert.AreEqual(r1.Value, 2);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
        [TestMethod]
        public void ContainsTryGet2()
        {
            var data = new[] { 1, 2, 3, 4 };
            var (xs, ys) = data.Aggregate((new Dict<int, int>(), new Dictionary<int, int>()), (a, x) =>
            {
                var (dic1, dic2) = a;
                dic1.Add(x + 1, x);
                dic2.Add(x + 1, x);
                return (dic1, dic2);
            });
            var r1 = xs.TryGetValue(-2);

            Assert.AreEqual(r1.HasValue, false);
            Assert.IsTrue(Helper.Compare(xs.Keys, data.Select(x => x + 1)));
            Assert.IsTrue(Helper.Compare(xs.Values, data));
            Assert.AreEqual(xs.Count, ys.Count);
            Assert.IsTrue(xs.IsValid());
        }
    }
}

