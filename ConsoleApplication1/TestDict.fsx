﻿#r @"..\packages\FsCheck.2.14.0\lib\net452\FsCheck.dll"
#r @"..\DevTests\bin\Debug\DevTests.exe"

// reset your interactive session

open FsCheck
open DevTests
open System.Collections
open System.Linq

let mkDict (xs : FSharp.Collections.List<'a>) : Dict<'a,'a> =
    List.fold (fun (d : Dict<'a,'a>) (x:'a) -> 
        d.Add(x,x)
        d) (Dict()) xs

let keysEqual (xs : FSharp.Collections.List<int>) = 
    let distinct = xs |> List.distinct
    let ys = mkDict(distinct).Keys |> Seq.toList
    distinct |> List.sort = ys
Check.Quick keysEqual

let valuesEqual (xs : FSharp.Collections.List<int>) = 
    let distinct = xs |> List.distinct
    let ys = mkDict(distinct).Values |> Seq.toList
    distinct |> List.sort = ys
Check.Quick valuesEqual

// any item in the list can be found
let find1 (x : int) (xs : FSharp.Collections.List<int>) (i : int) = 
    let distinct = (x::xs) |> List.distinct
    let iMod = abs i % (distinct |> List.length)
    let item = distinct.Item(iMod)
    let d = mkDict(distinct)
    (item = d.Item(item)) && (d.ContainsKey(item)) && (d.ContainsValue(item)) && (d.TryGetValue(item).HasValue)  && (d.TryGetValue(item).Value = item)
Check.Quick find1

// any item not in the list can't be found
let find2 (xs : FSharp.Collections.List<int>) (i : int) = 
    let distinct = xs |> List.distinct
    let max = distinct |> List.fold (fun a x -> if x > a then x else a ) 0
    let dict = mkDict(distinct)
    (false = dict.ContainsKey(max + 1)) && (false = dict.ContainsValue(max + 1)) && (dict.TryGetValue(max + 1).HasValue = false)
Check.Quick find2

// try removing stuff in the list
let remove1 (x : int) (xs : FSharp.Collections.List<int>) (i : int) = 
    let test1,test2 = 
        let distinct = (x::xs) |> List.distinct
        let iMod = abs i % (distinct |> List.length)
        let valueToRemove = distinct.Item(iMod)
        let dic = mkDict(distinct)
        let dic2 = 
            distinct |> List.fold (fun (d : Generic.Dictionary<int,int>) (x : int) -> 
                d.Add(x,x)
                d) (Generic.Dictionary<int,int>())    
        dic.Remove valueToRemove |> ignore
        dic2.Remove valueToRemove |> ignore
        (dic2,dic)        
    (test1.Keys.AsEnumerable() |> Seq.sort |> Seq.toList = (test2.Keys |> Seq.toList)) && (test1.Count = test2.Count)
Check.Quick remove1

// any item not in the list can't be found
let remove2 (xs:FSharp.Collections.List<int>) (i:int) = 
    let distinct = xs |> List.distinct
    let max = distinct |> List.fold (fun a x -> if x > a then x else a ) 0
    let dict = mkDict(distinct)
    (false = dict.Remove(max + 1)) && (dict.Values |> Seq.toList = (distinct |> List.sort)) && (distinct.Count() = dict.Count)
Check.Quick remove2



