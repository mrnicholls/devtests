﻿#r @"..\packages\FsCheck.2.14.0\lib\net452\FsCheck.dll"
#r @"..\DevTests\bin\Debug\DevTests.exe"

// reset your interactive session

open FsCheck
open DevTests
open System.Collections.Generic
open System.Linq

let revRevIsOrig (xs:list<int>) = List.rev(List.rev xs) = xs
Check.Quick revRevIsOrig

let compare (xs : List<'a>) (ys : SingleList<'a>) = 
    (xs |> Seq.toList = (ys.AsEnumerable() |> Seq.toList)) && (xs.Count = ys.Count)

let testException<'a,'b when 'a :> exn> (f : Unit -> 'b) : _ =
    try 
        f() |> ignore
        false
    with
        | :? 'a -> true

let listsEqual (xs:list<int>) = 
    let ys = SingleList(xs |> List.toSeq).AsEnumerable() |> Seq.toList
    xs = ys
Check.Quick listsEqual

let listsEqual3 (xs:list<string>) = 
    let ys = SingleList(xs |> List.toSeq).AsEnumerable() |> Seq.toList
    xs = ys
Check.Quick listsEqual3

let listsEqual4 (xs:list<obj>) = 
    let ys = SingleList(xs |> List.toSeq).AsEnumerable() |> Seq.toList
    xs = ys
Check.Quick listsEqual4

let listsEqual2 (xs:list<int>) (x:int) = 
    let ys = SingleList(xs)
    ys.Add x
    let zs = System.Collections.Generic.List(xs)
    zs.Add x
    compare zs ys
Check.Quick listsEqual2

let listsItems (xs:list<int>) = 
    let ys = SingleList(xs)
    [0..List.length xs - 1] |> List.fold (fun a i -> a && (xs.Item(i) = ys.Item(i))) true
Check.Quick listsItems

let listsClear (xs:list<int>) = 
    let ys = SingleList(xs)
    ys.Clear()
    let zs = System.Collections.Generic.List(xs)
    zs.Clear()
    compare zs ys
Check.Quick listsClear

let listsIndexOf (xs:list<int>) (x : int) = 
    let ys = SingleList(xs)
    let zs = System.Collections.Generic.List(xs)
    ys.IndexOf x = zs.IndexOf x
Check.Quick listsIndexOf

let listsInsert (xs:list<int>) (i : int) (x : int) = 
    let modI = (abs i) % ((xs |> (List.length)) + 1)
    let ys = SingleList(xs)
    ys.Insert(modI,x)
    let zs = System.Collections.Generic.List(xs)
    zs.Insert(modI,x)
    compare zs ys
Check.Quick listsInsert

let listsInsertException1 (xs:list<int>) (i : int) (x : int) = 
    let modI = List.length xs + (abs i) + 1
    let ys = SingleList(xs)
    testException<System.ArgumentException,_> (fun () -> ys.Insert(modI,x))
Check.Quick listsInsertException1

let listsInsertException2 (xs:list<int>) (i : int) (x : int) = 
    let modI = -(abs i) - 1
    let ys = SingleList(xs)
    testException<System.ArgumentException,_> (fun () -> ys.Insert(modI,x))
Check.Quick listsInsertException2

let listsRemove (xs:list<int>) (x : int) = 
    let ys = SingleList(xs)
    let boolys = ys.Remove(x)
    let zs = System.Collections.Generic.List(xs)
    let boolzs = zs.Remove(x)
    (compare zs ys) && (boolys = boolzs)
Check.Quick listsRemove

let listsRemoveAt (xs:list<int>) (i : int) = 
    if (xs.Length > 0) then
        let safei = (abs i) % (xs.Length)
        let ys = SingleList(xs)
        ys.RemoveAt(safei)
        let zs = System.Collections.Generic.List(xs)
        zs.RemoveAt(safei)
        (compare zs ys)
    else
        true
Check.Quick listsRemoveAt

let listsRemoveAtException (xs:list<int>) (i : int) (x : int) = 
    let modI = List.length xs + (abs i)
    let ys = SingleList(xs)
    testException<System.ArgumentException,_> (fun () -> ys.RemoveAt(modI))
Check.Quick listsRemoveAtException

let listsLINQ (xs:list<int>) = 
    let f x = SingleList([x;2*x]) 
    let g x = [x;2*x] |> List.toSeq

    let ys = SingleList(xs)
    let q1 = ((ys.SelectMany (fun y -> (f y))).Where (fun y -> y % 3 = 0)).AsEnumerable() |> Seq.toList
    let q2 = ((xs |> List.toSeq).SelectMany (fun y -> (g y))).Where (fun y -> y % 3 = 0) |> Seq.toList
    q1 = q2
Check.Quick listsLINQ

let listsIndexerException (xs:list<int>) (i:int) = 
    let modI = List.length xs + (abs i)
    let ys = SingleList(xs)
    testException<System.ArgumentException,_> (fun () -> ys.Item(modI))
Check.Quick listsIndexerException

