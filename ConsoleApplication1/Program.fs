﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
open DevTests

[<EntryPoint>]
let main argv = 
    #if INTERACTIVE
    #r @"C:\Users\m_r_n\source\repos\DevTestsSolution\DevTests\bin\Debug\DevTests.exe"
    #endif

    let x = Dict<int,int>()
 
    x.Add(5,5)
    x.Add(2,2)
    x.Add(1,1)
    x.Add(3,3)
    x.Add(7,7)
    x.Add(6,6)
    x.Add(4,4)

    let maybe = x.TryGetValue(4)
    let 4 = maybe.Value

    let y = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(4)
    let z = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(3)
    let a = x.AsEnumerable() |> Seq.toList
    let false = x.Remove(8)
    let b = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(7)
    let c = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(1)
    let d = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(2)
    let e = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(5)
    let f = x.AsEnumerable() |> Seq.toList
    let true = x.Remove(6)
    let g = x.AsEnumerable() |> Seq.toList

    printfn "%A" y
    0 // return an integer exit code


            