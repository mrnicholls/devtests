﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DevTests
{
    /// <summary>
    /// A singly-linked list
    /// Stores a list of items in the order they're added
    /// </summary>
    /// <typeparam name="T">Value type</typeparam>
    public partial class SingleList<T>
    {
        // public for testing...could be internal!
        public AListItemUnion head;
        // this is the last non empty item
#warning probably easier if its a Maybe<ListItem>
        AListItemUnion last;
        int count;

        internal SingleList(AListItemUnion head, AListItemUnion last, int count)
        {
            this.head = head;
            this.last = last;
            this.count = count;
        }

        public SingleList() : this(Empty.Value, Empty.Value, 0) { }

        public SingleList(IEnumerable<T> xs) : this(Empty.Value, Empty.Value, 0)
        {
            xs.Aggregate(
                0,
                (a, x) =>
                {
                    Add(x);
                    return a + 1;
                });
        }

        /// <summary>
        /// for testing purposes, a post condition on the state of a SingleList
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            var countValid = this.count == AListItemUnion.Count(head);
            var lastValid1 = this.last == AListItemUnion.ToEnumerableItems(head).Cast<AListItemUnion>().Skip(count - 1).DefaultIfEmpty(Empty.Value).First();
            var lastValid2 =
                this.last.Match(
                    item => item.Next == Empty.Value,
                    () => true);

            return countValid && lastValid1 && lastValid2;
        }

        /// <summary>
        /// adds the item to the end of the list, without updating count
        /// </summary>
        /// <param name="newItem"></param>
        void AddItem(ListItem newItem)
        {
            last.Match(
                item =>
                {
                    item.Next = newItem;
                    last = item.Next;
                    return false;
                },
                () =>
                {
                    head = newItem;
                    last = newItem;
                    return true;
                });
        }

        /// <summary>
        /// adds the item to the end of the list
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value)
        {
            AddItem(new ListItem(Empty.Value, value));

            count = count + 1;
        }

        /// <summary>
        /// get number of elements in list
        /// </summary>
        public int Count => count;


        /// <summary>
        /// turn an index into an option, so we can mentally work in a total world
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        static Option<int> GetIndex(int index,int count)
        {
            if ((index >= 0) && (index < count))
            {
                return Option<int>.Some(index);
            }
            else
            {
                return Option<int>.None();
            }
        }

        /// <summary>
        /// identity function or throw exception if index is invalid
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        static int GetIndexOrArgException(int index,int count)
        {
            var maybeIndex = GetIndex(index,count);
            if (maybeIndex.HasValue)
            {
                return maybeIndex.Value;
            }
            else
            {
                throw new ArgumentException("index should be >= 0 and < count");
            }
        }

        /// <summary>
        /// get, set value at specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get => TryGetItem(GetIndexOrArgException(index,count)).Value.Value;
            set => TryGetItem(GetIndexOrArgException(index,count)).Value.Value = value;
        }

        Option<ListItem> TryGetItem(int index) =>
            Find((_, i, _2) => i == index).Item1;

        /// <summary>
        /// find a item and its index, and what was processed (in reverse order).
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// this is a bit like a pure functional "Zipper" that remembers where its been.
        (Option<ListItem>, int,Stack<ListItem>) Find(Func<ListItem,int, Stack<ListItem>,bool> pred)
        {
            // note there is an imperative trick here
            // the stack in the default value is the stack of the last
            // unsuccessful iteration...so we're actually updating it, rather than
            // threading it through the Scan
            var stack = new Stack<ListItem>();
            var @default = (Option<ListItem>.None(), -1,stack);

            return
                AListItemUnion.ScanItems(
                    this.head,
                    (a, item2) =>
                    {
                        var (maybeItem, index, processed) = a;

                        if (pred(item2,index,processed))
                        {
                            return (Option<ListItem>.Some(item2), index, processed);
                        }
                        else
                        {
                            processed.Push(item2);
                            return (maybeItem, index + 1, processed);
                        }
                    },
                    (Option<ListItem>.None(), 0, stack)).
                        SkipWhile(x => !x.Item1.HasValue).
                        DefaultIfEmpty(@default).
                        First();
        }

        /// <summary>
        /// find a item and its index, and what was processed (in reverse order).
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// this is a bit like a pure functional "Zipper" that remembers where its been.
        (Option<ListItem>, int, Stack<ListItem>) Find(T item)
        {
            return Find((item2, _, _2) => EqualityComparer<T>.Default.Equals(item2.Value, item));
        }

        /// <summary>
        /// searches list in order for first item matching specified item and returns 0 based index (-1 if not found)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(T item)
        {
            var (_,i,_) = Find(item);

            return i;
        }

        /// <summary>
        /// insert new item at specific 0 based index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item)
        {
            var safeIndex = GetIndexOrArgException(index, count + 1);

            var (maybeItem,_,previous) = 
                Find((_, i, _2) => i == safeIndex);

            var newItem =
                new ListItem(
                    maybeItem.HasValue ? (AListItemUnion)maybeItem.Value : Empty.Value,
                    item);

            if (!previous.Any())
            {
                this.head = newItem;
            }
            else
            {
                previous.Peek().Next = newItem;
            }

            if (!maybeItem.HasValue)
            {
                this.last = newItem;
            }

            count = count + 1;
        }

        /// <summary>
        /// remove item as specified 0 based index
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            var safeIndex = GetIndexOrArgException(index, count);
            var (maybeItem, _, previous) =
                Find((_, i, _2) => i == safeIndex);

            if (previous.Any())
            {
                var prev = previous.Peek();
                prev.Next = maybeItem.Value.Next;
                this.last = prev.Next.Match(
                    item => this.last, 
                    () => prev);
            }
            else
            {
                this.head = maybeItem.Value.Next;
                this.last = this.head.Match(item => this.last, () => Empty.Value);
            }
            
            count = count - 1;
        }

        /// <summary>
        /// remove all elements
        /// </summary>
        public void Clear()
        {
            this.head = Empty.Value;
            this.last = this.head;
            this.count = 0;
        }

        /// <summary>
        /// determines whether item is in list
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item) => this.Find(item).Item1.HasValue;

        /// <summary>
        /// remove 1st specified item from list, and return true, or false if not found
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            var (maybeItem,_,processed) = Find(item);

            if (maybeItem.HasValue)
            {
                AListItemUnion lastCandidate;

                if (processed.Any())
                {
                    processed.Peek().Next = maybeItem.Value.Next;
                    lastCandidate = processed.Peek();
                }
                else
                {
                    this.head = maybeItem.Value.Next;
                    lastCandidate = Empty.Value;
                }

                if (maybeItem.Value == last)
                {
                    last = lastCandidate;
                }

                count = count - 1;
            }

            return maybeItem.HasValue;
        }

        /// <summary>
        /// return enumerator that iterates through list
        /// </summary>
        /// <returns></returns>
        // cheating? see instructions.txt
        public IEnumerator<T> GetEnumerator() => 
            this.AsEnumerable().GetEnumerator();

        /// <summary>
        /// return an IEnumerable (this is not normally what AsEnumerable does)
        /// </summary>
        /// <returns></returns>
        // cheating? see instructions.txt
        public IEnumerable<T> AsEnumerable() => 
            AListItemUnion.AsEnumerable(head);
    }
}
