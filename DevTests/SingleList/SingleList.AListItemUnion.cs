﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevTests
{
    partial class SingleList<T>
    {
        // a bit like a visitor (ie encoding of a union).
        public abstract class AListItemUnion
        {
            /// <summary>
            /// How many items past this one (including this one).
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static int Count(AListItemUnion item) => Fold(item, (st, _) => st + 1, 0);

            /// <summary>
            /// "Scan" an from an listitem, over the listitems themselves (as they are mutable)
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static IEnumerable<TState> ScanItems<TState>(
                AListItemUnion item,
                Func<TState, ListItem, TState> folder,
                TState seed)
            {
                var maybeItemAndContinue = new { maybeItem = new ListItem[] { }, isDone = false };
                yield return seed;

                while (!(maybeItemAndContinue = item.Match(
                        x => new { maybeItem = new ListItem[] { x }, isDone = false },
                        () => new { maybeItem = new ListItem[] { }, isDone = true })).isDone)
                {
                    foreach (var li in maybeItemAndContinue.maybeItem)
                    {
                        seed = folder(seed, li);

                        yield return seed;
                        item = li.Next;
                    }
                }
            }

            /// <summary>
            /// "Scan" an from an listitem, over the values in the items
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static IEnumerable<TState> Scan<TState>(AListItemUnion item, Func<TState, T, TState> folder, TState seed)
            {
                Func<TState, ListItem, TState> itemFolder =
                    (st, li) => folder(st, li.Value);

                return ScanItems(item, itemFolder, seed);
            }

            /// <summary>
            /// fold items
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static TState FoldItems<TState>(
                AListItemUnion item,
                Func<TState, ListItem, TState> folder,
                TState seed) => ScanItems(item, folder, seed).Last();

            /// <summary>
            /// fold values
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static TState Fold<TState>(
                AListItemUnion item,
                Func<TState, T, TState> folder,
                TState seed) => Scan(item, folder, seed).Last();

            /// <summary>
            /// create an enumerable of ListItems
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static IEnumerable<ListItem> ToEnumerableItems(AListItemUnion item)
            {
                // scan the items in the list returning "Some(item)" (as an array) if its an item
                // else "Nothing" (as an empty array).
                Func<AListItemUnion, ListItem[]> asItemMaybe =
                    x => x.Match(
                        li => new[] { li },
                        () => new ListItem[] { });

                // "scan" the answer, yielding ListItems
                foreach (var maybeItem in ScanItems(item, (_, li) => asItemMaybe(li), new ListItem[] { }))
                {
                    foreach (var isItem in maybeItem)
                    {
                        yield return isItem;
                    }
                }
            }

            /// <summary>
            /// create an enumerable of ListItem values
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static IEnumerable<T> AsEnumerable(AListItemUnion item)
            {
                return (from li in AListItemUnion.ToEnumerableItems(item)
                        select li.Value);
            }

            // this "visitor"/match function.
            public abstract U Match<U>(Func<ListItem, U> isListItem, Func<U> isNone);
        }

        public class ListItem : AListItemUnion
        {
            internal AListItemUnion Next;
            internal T Value;

            public ListItem(AListItemUnion next, T value)
            {
                Next = next;
                Value = value;
            }

            public override U Match<U>(Func<ListItem, U> isListItem, Func<U> isNone) => isListItem(this);
        }

        public class Empty : AListItemUnion
        {
            public static Empty Value = new Empty();

            // has to be private to make all instances coincident (and make == work)....for OOers - singleton pattern
            private Empty() { }

            public override U Match<U>(Func<ListItem, U> isListItem, Func<U> isNone) => isNone();
        }
    }
}
