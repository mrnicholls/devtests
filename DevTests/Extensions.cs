﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DevTests
{
    public static class DoubleListExtensions
    {
        public static DoubleList<T> Unit<T>(T value)
        {
            return new DoubleList<T>(new T[] { value });
        }

        public static DoubleList<U> SelectMany<T, U>(
            this DoubleList<T> @this,
            Func<T, DoubleList<U>> f)
        {
            return @this.SelectMany(f, (_, u) => u);
        }
        public static DoubleList<U> Select<T, U>(
            this DoubleList<T> @this,
            Func<T, U> f)
        {
            return @this.SelectMany(t => Unit(f(t)));
        }

        public static DoubleList<V> SelectMany<T, U, V>(
            this DoubleList<T> @this,
            Func<T, DoubleList<U>> f,
            Func<T, U, V> g)
        {
            var empty = (DoubleList<V>.AListItemUnion)DoubleList<V>.Empty.Value;
            // fold over the list, generating a new list, the tail as we go.
            var headAndTail =
                DoubleList<T>.AListItemUnion.Fold(
                        @this.head,
                        (a, t) =>
                            DoubleList<U>.AListItemUnion.Fold(
                                f(t).head,
                                (a1, u) =>
                                {
                                    a1.Add(g(t, u));
                                    return a1;
                                },
                                a),
                        new DoubleList<V>());

            return headAndTail;
        }

        public static DoubleList<T> Where<T>(this DoubleList<T> @this, Func<T, bool> predicate)
        {
            return DoubleList<T>.AListItemUnion.Fold(
                @this.head,
                (a, t) =>
                {
                    if (predicate(t))
                    {
                        a.Add(t);
                    }
                    return a;
                },
                new DoubleList<T>());
        }
    }

    public static class SingleListExtensions
    {
        public static SingleList<T> Unit<T>(T value)
        {
            return new SingleList<T>(new T[] { value });
        }

        public static SingleList<T> Where<T>(this SingleList<T> @this, Func<T, bool> predicate)
        {
            return SingleList<T>.AListItemUnion.Fold(
                @this.head,
                (a, t) =>
                {
                    if (predicate(t))
                    {
                        a.Add(t);
                    }
                    return a;
                },
                new SingleList<T>());
        }

        public static SingleList<U> SelectMany<T, U>(
            this SingleList<T> @this,
            Func<T, SingleList<U>> f) => @this.SelectMany(f, (_, u) => u);
        
        public static SingleList<U> Select<T, U>(
            this SingleList<T> @this,
            Func<T, U> f) => @this.SelectMany(t => Unit(f(t)));
        

        public static SingleList<V> SelectMany<T, U, V>(
            this SingleList<T> @this,
            Func<T, SingleList<U>> f,
            Func<T, U, V> g)
        {
            var empty = (SingleList<V>.AListItemUnion)SingleList<V>.Empty.Value;
            // fold over the list, generating a new list, the tail as we go.
            var headAndTail =
                SingleList<T>.AListItemUnion.Fold(
                        @this.head,
                        (a, t) =>
                            SingleList<U>.AListItemUnion.Fold(
                                f(t).head,
                                (a1, u) =>
                                {
                                    a1.Add(g(t, u));
                                    return a1;
                                },
                                a),
                        new SingleList<V>());

            return headAndTail;
        }
    }
}
