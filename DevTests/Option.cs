﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevTests
{
    public static class OptionX
    {
        public static Option<T> Unit<T>(T value) => Option<T>.Some(value);
        public static Option<U> Select<T, U>(this Option<T> @this, Func<T, U> f) => Option<T>.Map<U>(@this, f);
        public static Option<U> SelectMany<T, U>(
            this Option<T> @this,
            Func<T, Option<U>> f) => SelectMany<T, U, U>(@this, f, (t, u) => u);
        public static Option<V> SelectMany<T, U, V>(
            this Option<T> @this,
            Func<T, Option<U>> f,
            Func<T, U, V> g)
        {
            if (@this.HasValue)
            {
                var t = @this.Value;
                return Option<U>.Map<V>(f(t), u => g(t, u));
            }
            else
            {
                return Option<V>.None();
            }
        }
    }

    public class Option<T>
    {
        public readonly T Value;
        public readonly bool HasValue;

        Option(T value)
        {
            Value = value;
            HasValue = true;
        }

        Option()
        {
            HasValue = false;
        }

        public static Option<T> Some(T value) =>
            new Option<T>(value);

        public static Option<T> None() =>
            new Option<T>();

        public static Option<U> Map<U>(Option<T> opt,Func<T,U> f)
        {
            if (opt.HasValue)
            {
                return new Option<U>(f(opt.Value));
            }
            else
            {
                return new Option<U>();
            }
        }
    }
}
