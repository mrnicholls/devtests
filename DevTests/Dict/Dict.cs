﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevTests
{
    /// <summary>
    /// A key-value store
    /// Uses a binary tree to efficiently store items in-order
    /// </summary>
    /// <typeparam name="K">Key type</typeparam>
    /// <typeparam name="V">Value type</typeparam>
    public partial class Dict<K, V>
    {
        // for testing!
        ADictItemUnion root = Empty.Value;
        int count = 0;

        /// <summary>
        /// for testing purposes, a post condition on the state of a SingleList
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            var countValid = this.count == ADictItemUnion.AsEnumerableAscending(root).Count();
            var linksValid =
                !(from item in ADictItemUnion.AsEnumerableAscending(root)
                 where item.Left.Match(
                     i => Comparer<K>.Default.Compare(item.Key, i.Key) < 0,
                     () => false) &&
                     item.Right.Match(
                         i => Comparer<K>.Default.Compare(item.Key, i.Key) > 0,
                         () => false)
                 select item).Any();

            return countValid && linksValid;
        }

        public V this[K key]
        {
            get
            {
                var maybe = TryGetValue(key);
                if (maybe.HasValue)
                {
                    return maybe.Value;
                }
                else
                {
                    throw new KeyNotFoundException();
                }
            }

            set
            {
                var maybe = ADictItemUnion.TryGet(root,key);
                if (maybe.HasValue)
                {
                    maybe.Value.Value = value;
                }
                else
                {
                    throw new KeyNotFoundException();
                }
            }
        }

        public int Count 
        {
            get => count;
        }

        public IEnumerable<K> Keys
        {
            get
            {
                return
                    from tuple in this.AsEnumerable()
                    select tuple.Item1;
            }
        }

        public IEnumerable<V> Values
        {
            get
            {
                return 
                    from tuple in this.AsEnumerable()
                    select tuple.Item2;
            }
        }

        public void Add(Tuple<K, V> item)
        {
            Add(item.Item1,item.Item2);
        }

        public void Add(K key, V value)
        {
            root = ADictItemUnion.Add(root,key,value);
            count = count + 1;
        }

        public void Clear()
        {
            this.root = Empty.Value;
            count = 0;
        }

        /// <summary>
        /// compare values using EqualityComparer<V>.Default
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool ContainsValue(V item)
        {
            // do it by brute force, there is no shortcut
            return this.AsEnumerable().Any(tuple => EqualityComparer<V>.Default.Equals(tuple.Item2,item));
        }

        /// <summary>
        /// Compares keys with Comparer<K>.Default.Compare
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(K key) => ADictItemUnion.TryGet(root,key).HasValue;

        /// <summary>
        /// return enumerable "inorder"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tuple<K, V>> AsEnumerable()
        {
            return
                from item in ADictItemUnion.AsEnumerableAscending(root)
                select Tuple.Create(item.Key, item.Value);
        }

        /// <summary>
        /// return enumerator "inorder"
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Tuple<K, V>> GetEnumerator()
        {
            return this.AsEnumerable().GetEnumerator();
        }

        public bool Remove(K key)
        {
            var answer = ADictItemUnion.Remove(root, key);
            root = answer.Item2;
            count = answer.Item1 ? count - 1 : count;
            return answer.Item1;
        }

        public Option<V> TryGetValue(K key) => 
            Option<DictItem>.Map<V>(ADictItemUnion.TryGet(this.root,key),item => item.Value);

        // for testing
        public Option<V> RootValue() =>
            root.Match(
                x => Option<V>.Some(x.Value),
                () => Option<V>.None());
    }
}


