﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DevTests
{
    /* ADictItemUnion, DictItem and Empty are a (almost) functionally pure implementation of a binary search tree 
       all implementations use "tail recursive" code mapped to while loops in a stylalised manner, where state is
       threaded through the loop until complete (the implementation DOES allow values in a the structure to be 
       ammended - rather than regenerate) */
    partial class Dict<K, V>
    {
        /// <summary>
        /// base class that represents the discrimated union of a Node and Empty
        /// </summary>
        abstract class ADictItemUnion
        {
            /// <summary>
            /// try to find a value, return none if not found
            /// uses Comparer<K>.Default.Compare(dict.Key, key) to compare keys
            /// </summary>
            /// <param name="item"></param>
            /// <param name="key"></param>
            /// <returns></returns>
            internal static Option<DictItem> TryGet(ADictItemUnion item, K key)
            {
                // "tail recursive" implementation that threads this state
                // start with current item, searching left/right to find matching key
                var state = new { isDone = false, next = item, answer = Option<DictItem>.None() };

                while (!state.isDone)
                {
                    state = state.next.Match(
                        dict =>
                        {
                            var diff = Comparer<K>.Default.Compare(dict.Key, key);

                            if (diff == 0)
                            {
                                return new { isDone = true, next = state.next, answer = Option<DictItem>.Some(dict) };
                            }
                            else
                            {
                                if (diff < 0)
                                {
                                    return new { isDone = false, next = dict.Left, answer = state.answer };
                                }
                                else
                                {
                                    return new { isDone = false, next = dict.Right, answer = state.answer };
                                }
                            }
                        },
                        () => new { isDone = true, next = state.next, answer = state.answer });
                }
                return state.answer;
            }

            /// <summary>
            /// used in remove, gets the rightmost item, and returns it, together with a reconstructed tree
            /// with this item removed (pure)
            /// </summary>
            /// <param name="root"></param>
            /// <returns></returns>
            static IEnumerable<((K, V), ADictItemUnion)> RightMostMaybe(ADictItemUnion root)
            {
                var state = new
                {
                    current = root,
                    foundMaybe = new DictItem[] { },
                    isDone = false,
                    newTree = new Func<ADictItemUnion, ADictItemUnion>(x => x)
                };

                while (!state.isDone)
                {
                    var newTree = state.newTree;
                    state = state.current.Match(
                        item =>
                            item.Right.Match(
                                itemRight =>
                                    new
                                    {
                                        current = item.Right,
                                        foundMaybe = state.foundMaybe,
                                        isDone = false,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                            right => newTree(new DictItem(item.Key, item.Value, item.Left, right)))
                                    },
                                () =>
                                    new
                                    {
                                        current = state.current,
                                        foundMaybe = new[] { item },
                                        isDone = true,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                            empty => newTree(item.Left))
                                    }),
                            () =>
                                new
                                {
                                    current = state.current,
                                    foundMaybe = state.foundMaybe,
                                    isDone = true,
                                    newTree = newTree
                                });
                }

                return
                    from found in state.foundMaybe
                    select ((found.Key, found.Value), state.newTree(Empty.Value));
            }

            /// <summary>
            /// removes a item based on its key
            /// </summary>
            /// <param name="root"></param>
            /// <param name="key"></param>
            /// <returns></returns>
            public static (bool, ADictItemUnion) Remove(ADictItemUnion root, K key)
            {
                // see Introduction to Algorithms, Thomas H. Cormen, Charles E. Leiserson, Ronald L.Rivest and Clifford Stein, 3rd edition
                // or google delete binary search tree e.g. https://en.wikipedia.org/wiki/Binary_search_tree
                // much simpler to reconstruct the tree without the element
                // we search for the element,
                // if we find it, we replace it with the "next" inorder item
                // and we remove this item from the ">" subtree
                var state = new
                {
                    current = root,
                    isFound = false,
                    isDone = false,
                    newTree = new Func<ADictItemUnion, ADictItemUnion>(x => x)
                };

                while (!state.isDone)
                {
                    var newTree = state.newTree;
                    state = state.current.Match(
                        // found an item
                        item =>
                        {
                            var diff = Comparer<K>.Default.Compare(item.Key, key);

                            // if its less than then go left..
                            if (diff < 0)
                            {
                                return new
                                {
                                    current = item.Left,
                                    isFound = false,
                                    isDone = false,
                                    newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                        // and reconstruct this node, with whatever happens further down the tree
                                        left => newTree(new DictItem(item.Key, item.Value, left, item.Right)))
                                };
                            }
                            // if more go right
                            else if (diff > 0)
                            {
                                return new
                                {
                                    current = item.Right,
                                    isFound = false,
                                    isDone = false,
                                    newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                        // and reconstruct this node, with whatever happens further down the tree
                                        right => newTree(new DictItem(item.Key, item.Value, item.Left, right)))
                                };
                            }
                            else
                            {
                                var rightMostMaybe = RightMostMaybe(item.Left);

                                // if a hit
                                // then find the next "in order" element (which will be on the right).
                                // it can't have anything on the left (because its the next in order one)
                                //var nextMaybe = AsEnumerableAscending(item).Skip(1).Take(1);

                                // there isnt one...so we only have a left.
                                if (!rightMostMaybe.Any())
                                {
                                    return new
                                    {
                                        current = state.current,
                                        isFound = true,
                                        isDone = true,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                            empty => newTree(item.Right))
                                    };
                                }
                                else
                                {
                                    // there is one...so get it..
                                    var next = rightMostMaybe.First().Item1;
                                    // and generate the right tree with it removed
                                    var leftTree = rightMostMaybe.First().Item2;
                                    return new
                                    {
                                        current = state.current,
                                        isFound = true,
                                        isDone = true,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(
                                            empty => newTree(new DictItem(next.Item1, next.Item2, leftTree, item.Right)))
                                    };
                                }
                            }
                        },
                        // found an empty node
                        // so not found, and return the source tree as the target
                        () => new
                        {
                            current = state.current,
                            isFound = false,
                            isDone = true,
                            newTree = new Func<ADictItemUnion, ADictItemUnion>(_ => root)
                        });
                }

                return (state.isFound, state.newTree(Empty.Value));
            }

            internal static IEnumerable<DictItem> AsEnumerableAscending(
                ADictItemUnion item)
            {
                // translation of the following f# code into c#

                //         let rec TraverseInOrder < 'a,'t > : Tree<'t> -> seq<Node<'t>> =
                //            fun node->
                //                // we recurse through the tree, if we find something on the left
                //                // then we move to it, and save it for later in the stack.
                //                // we only ever process a node IF we have no 
                //                let rec loop : Tree < 't> -> List<Node<'t >> ->seq<Node<'t>> -> seq<Node<'t>> =
                //                        fun current stack acc2 ->
                //                            match(current, stack) with
                //                            | (Empty,[])     -> acc2
                //                            | (Empty, n::ls) -> loop (n.right) ls (seq { yield n; yield! acc2; })
                //                            | (Node n, _)    -> loop (n.left) (n::stack) acc2
                //            loop node[] Seq.empty

                // see Introduction to Algorithms, Thomas H. Cormen, Charles E. Leiserson, Ronald L.Rivest and Clifford Stein, 3rd edition
                // or google delete binary search tree e.g. https://en.wikipedia.org/wiki/Binary_search_tree

                // we find the right hand most node in a tree, saving the items in the spine as we go
                // once we have found it, we process and then recusively apply the same process to the left tree
                // until we exhaust it, then recursively work back through our stacked up stack

                // set up state that we will update through the loop
                // (note: "todo" is mutable, but we include it in this "state" for clarity).
                // PS. it IS possible to do this without the use of "Stack", but this implementation is clearer
                var state = new
                {
                    current = item,
                    todo = new Stack<DictItem>(),
                    toYield = new DictItem[] { },
                    isDone = false
                };

                while (!state.isDone)
                {
                    state = state.current.Match(
                        // if we've found a ndoe, there will
                        // be a bigger on to the right
                        // so select that one, and save this one until later.
                        node =>
                        {
                            state.todo.Push(node);

                            return new
                            {
                                current = node.Right,
                                todo = state.todo,
                                toYield = new DictItem[] { },
                                isDone = false
                            };
                        },
                        () =>
                        {
                            // we must have found the node furthers to the right (biggest)
                            // if we don't have any more to do, then we can exit
                            if (!state.todo.Any())
                            {
                                return new
                                {
                                    current = state.current,
                                    todo = state.todo,
                                    toYield = new DictItem[] { },
                                    isDone = true
                                };
                            }
                            // we have found a new starting point to
                            // process from, it must be bigger than any of the others left
                            // so process it, and then 
                            else
                            {
                                var head = state.todo.Pop();

                                return new
                                {
                                    current = head.Left,
                                    todo = state.todo,
                                    toYield = new DictItem[] { head },
                                    isDone = false
                                };
                            }
                        });

                    foreach (var y in state.toYield)
                    {
                        yield return y;
                    }
                }
            }

            /// <summary>
            /// create a new tree with the key value pair added (pure).
            /// </summary>
            /// <param name="root"></param>
            /// <param name="key"></param>
            /// <param name="value"></param>
            /// <returns></returns>
            public static ADictItemUnion Add(
                ADictItemUnion root,
                K key,
                V value)
            {
                // see Introduction to Algorithms, Thomas H. Cormen, Charles E. Leiserson, Ronald L.Rivest and Clifford Stein, 3rd edition
                // new elements added to the fringe of the tree.
                // accumulator is a continuation, that constructs the new tree, based on the new branche
                var state = new
                {
                    current = root,
                    newTree = new Func<ADictItemUnion, ADictItemUnion>(x => x),
                    isDone = false
                };

                while (!state.isDone)
                {
                    var newTree = state.newTree;
                    state = state.current.Match(
                        item =>
                        {
                            var diff = Comparer<K>.Default.Compare(item.Key, key);

                            if (diff == 0)
                            {
                                return new
                                {
                                    current = state.current,
                                    newTree = new Func<ADictItemUnion, ADictItemUnion>(_ => { throw new Exception("key exists!"); }),
                                    isDone = true
                                };
                            }
                            else
                            {
                                if (diff < 0)
                                {
                                    return new
                                    {
                                        current = item.Left,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(left => newTree(new DictItem(item.Key, item.Value, left, item.Right))),
                                        isDone = false
                                    };
                                }
                                else
                                {
                                    return new
                                    {
                                        current = item.Right,
                                        newTree = new Func<ADictItemUnion, ADictItemUnion>(right => newTree(new DictItem(item.Key, item.Value, item.Left, right))),
                                        isDone = false
                                    };
                                }
                            }
                        },
                        () =>
                        new
                        {
                            current = state.current,
                            newTree = new Func<ADictItemUnion, ADictItemUnion>(_empty => newTree(new DictItem(key, value, Empty.Value, Empty.Value))),
                            isDone = true
                        });
                }

                return state.newTree(Empty.Value);
            }

            // this "visitor"/match function.
            public abstract U Match<U>(Func<DictItem, U> isItem, Func<U> isNone);
        }

        class DictItem : ADictItemUnion
        {
            public readonly K Key;
            // !!! this value is allowed to be ammended
            public V Value;
            public readonly ADictItemUnion Left;
            public readonly ADictItemUnion Right;

            public DictItem(K key, V value, ADictItemUnion left, ADictItemUnion right)
            {
                Key = key;
                Value = value;
                Left = left;
                Right = right;
            }

            public override U Match<U>(Func<DictItem, U> isItem, Func<U> isNone)
            {
                return isItem(this);
            }
        }

        class Empty : ADictItemUnion
        {
            public static Empty Value = new Empty();

            // has to be private to make all instances coincident (and make == work)....for OOers - singleton pattern
            private Empty() { }

            public override U Match<U>(Func<DictItem, U> isItem, Func<U> isNone)
            {
                return isNone();
            }
        }
    }
}
