﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DevTests
{
    // contains inner data structure for a double linked list.
    // quite imperative in nature (inherently)
    // implementation pretty much follows SingleList, but with its own implementation (for clarity/reduced complexity).
    partial class DoubleList<T>
    {
        // a bit like a visitor (ie encoding of a union).
        // public for testing purposes
        public abstract class AListItemUnion
        {
            /// <summary>
            /// How many items past this one (including this one).
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static int Count(AListItemUnion item) => Fold(item, (st, _) => st + 1, 0);

            /// <summary>
            /// ensure the links match up, for testing purposes
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static bool IsValid(AListItemUnion item)
            {
                return ToEnumerableItems(item).All(
                    li =>
                    {
                        return li.Next.Match(
                            next => (next.Prev == li) && (li.Next == next),
                            () => true);
                    });
            }

            /// <summary>
            /// "Scan" an from an listitem, over the listitems themselves (as they are mutable)
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static IEnumerable<TState> ScanItems<TState>(
                AListItemUnion item,
                Func<TState, ListItem, TState> folder,
                TState seed)
            {
                // state to be threaded.
                var maybeItemAndContinue = new { maybeItem = new ListItem[] { }, isDone = false };
                yield return seed;

                while (!maybeItemAndContinue.isDone)
                {
                    maybeItemAndContinue = item.Match(
                        x => new { maybeItem = new ListItem[] { x }, isDone = false },
                        () => new { maybeItem = new ListItem[] { }, isDone = true });

                    foreach (var li in maybeItemAndContinue.maybeItem)
                    {
                        seed = folder(seed, li);

                        yield return seed;
                        item = li.Next;
                    }
                }
            }

            /// <summary>
            /// "Scan" an from an listitem, over the values in the items
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static IEnumerable<TState> Scan<TState>(AListItemUnion item, Func<TState, T, TState> folder, TState seed)
            {
                Func<TState, ListItem, TState> itemFolder =
                    (st, li) => folder(st, li.Value);

                return ScanItems(item, itemFolder, seed);
            }

            /// <summary>
            /// fold values
            /// </summary>
            /// <typeparam name="TState"></typeparam>
            /// <param name="item"></param>
            /// <param name="folder"></param>
            /// <param name="seed"></param>
            /// <returns></returns>
            internal static TState Fold<TState>(
                AListItemUnion item, 
                Func<TState, T, TState> folder, 
                TState seed) => Scan(item, folder, seed).Last();

            /// <summary>
            /// create an enumerable of ListItems
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static IEnumerable<ListItem> ToEnumerableItems(AListItemUnion item)
            {
                // scan the items in the list returning "Some(item)" (as an array) if its an item
                // else "Nothing" (as an empty array).
                Func<AListItemUnion, ListItem[]> asItemMaybe =
                    x => x.Match(li => new[] { li },
                    () => new ListItem[] { });

                // "scan" the answer, yielding ListItems
                foreach (var maybeItem in ScanItems(item, (_, li) => asItemMaybe(li), new ListItem[] { }))
                {
                    foreach (var isItem in maybeItem)
                    {
                        yield return isItem;
                    }
                }
            }

            /// <summary>
            /// create an enumerable of ListItem values
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            internal static IEnumerable<T> ToEnumerable(AListItemUnion item)
            {
                return (from li in AListItemUnion.ToEnumerableItems(item)
                        select li.Value);
            }

            // this "visitor"/match function.
            public abstract U Match<U>(Func<ListItem, U> isListItem, Func<U> isNone);
        }

        // public for test purposes
        public class ListItem : AListItemUnion
        {
            public AListItemUnion Next;
            public T Value;
            public AListItemUnion Prev;

            public ListItem(AListItemUnion prev, AListItemUnion next, T value)
            {
                Next = next;
                Prev = prev;
                Value = value;
            }

            public override U Match<U>(
                Func<ListItem, U> isListItem, 
                Func<U> isNone) => isListItem(this);
        }

        // public for testing purpose
        public class Empty : AListItemUnion
        {
            public static Empty Value = new Empty();

            // has to be private to make all instances coincident (and make == work)....for OOers - singleton pattern
            private Empty() { }

            public override U Match<U>(Func<ListItem, U> isListItem, Func<U> isNone) => isNone();            
        }
    }
}
