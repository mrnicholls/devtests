﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DevTests
{
    /// <summary>
    /// A doubly-linked list
    /// </summary>
    /// <typeparam name="T">Value type</typeparam>
    public partial class DoubleList<T>
    {
        // public for test purposes
        public AListItemUnion head;
        // this is the last non empty item
#warning this should probably be a Maybe<ListItem>?
        AListItemUnion last;
        int count;

        internal DoubleList(AListItemUnion head, AListItemUnion last, int count)
        {
            this.head = head;
            this.last = last;
            this.count = count;
        }

        public DoubleList()
        {
            head = Empty.Value;
            last = head;
            count = 0;
        }
        public DoubleList(IEnumerable<T> xs) : this()
        {
            xs.Aggregate(
                0,
                (a, x) =>
                {
                    Add(x);
                    return a + 1;
                });
        }

        /// <summary>
        /// for testing purposes, a post condition on the state of a SingleList
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            var countValid = this.count == AListItemUnion.Count(head);
            var lastValid1 = this.last == AListItemUnion.ToEnumerableItems(head).Cast<AListItemUnion>().Skip(count - 1).DefaultIfEmpty(Empty.Value).First();
            var lastValid2 =
                this.last.Match(
                    item => item.Next == Empty.Value,
                    () => true);
            var linksValid = AListItemUnion.IsValid(head);

            return countValid && lastValid1 && lastValid2 && linksValid;
        }

        /// <summary>
        /// adds the item to the end of the list, without updating count
        /// </summary>
        /// <param name="newItem"></param>
        void AddItem(ListItem newItem)
        {
            last.Match(
                item =>
                {
                    item.Next = newItem;
                    newItem.Prev = last;
                    last = item.Next;
                    return false;
                },
                () =>
                {
                    head = newItem;
                    last = newItem;
                    return true;
                });
        }

        /// <summary>
        /// adds the item to the end of the list
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value)
        {
            AddItem(
                new ListItem(
                    last.Match<AListItemUnion>(x => x,() => Empty.Value), 
                    Empty.Value, 
                    value));

            count = count + 1;
        }

        /// <summary>
        /// get number of elements in list
        /// </summary>
        public int Count => count;

        /// <summary>
        /// turn an index into an option, so we can mentally work in a total world
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        static Option<int> GetIndex(int index, int count)
        {
            if ((index >= 0) && (index < count))
            {
                return Option<int>.Some(index);
            }
            else
            {
                return Option<int>.None();
            }
        }

        /// <summary>
        /// identity function or throw exception if index is invalid
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        static int GetIndexOrArgException(int index, int count)
        {
            var maybeIndex = GetIndex(index, count);
            if (maybeIndex.HasValue)
            {
                return maybeIndex.Value;
            }
            else
            {
                throw new ArgumentException("index should be >= 0 and < count");
            }
        }

        /// <summary>
        /// get, set value at specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get => TryGetItem(GetIndexOrArgException(index, count)).Value.Value;
            set => TryGetItem(GetIndexOrArgException(index, count)).Value.Value = value;
        }

        Option<ListItem> TryGetItem(int index) =>
            Find((_, i) => i == index).Item1;

        /// <summary>
        /// find a item and its index, and what was processed (in reverse order).
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// this is a bit like a pure functional "Zipper" that remembers where its been.
        (Option<ListItem>, int) Find(Func<ListItem, int, bool> pred)
        {
            var @default = (Option<ListItem>.None(), -1);

            return
                AListItemUnion.ScanItems(
                    this.head,
                    (a, item2) =>
                    {
                        var (maybeItem, index) = a;

                        if (pred(item2, index))
                        {
                            return (Option<ListItem>.Some(item2), index);
                        }
                        else
                        {
                            return (maybeItem, index + 1);
                        }
                    },
                    (Option<ListItem>.None(), 0)).
                        SkipWhile(x => !x.Item1.HasValue).
                        DefaultIfEmpty(@default).
                        First();
        }

        /// <summary>
        /// find a item and its index, and what was processed (in reverse order).
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// this is a bit like a pure functional "Zipper" that remembers where its been.
        (Option<ListItem>, int) Find(T item)
        {
            return Find((item2, _) => EqualityComparer<T>.Default.Equals(item2.Value, item));
        }

        /// <summary>
        /// searches list in order for first item matching specified item and returns 0 based index (-1 if not found)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(T item)
        {
            var (_, i) = Find(item);

            return i;
        }

        /// <summary>
        /// insert new item at specific 0 based index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item)
        {
            var maybeItem = TryGetItem(GetIndexOrArgException(index, count + 1));

            var newItem =
                new ListItem(
                    // default prev to empty
                    Empty.Value,
                    maybeItem.HasValue ? (AListItemUnion)maybeItem.Value : Empty.Value,
                    item);

            if (!maybeItem.HasValue)
            {
                // its the end of the list.
                newItem.Prev =
                    this.last.Match(
                        last =>
                        {
                            last.Next = newItem;
                            return (AListItemUnion)last;
                        },
                        () =>
                        {
                            this.head = newItem;
                            return Empty.Value;
                        });
                this.last = newItem;
            }
            else
            {
                maybeItem.Value.Prev.Match(
                    prev =>
                    {
                        return prev.Next = newItem;
                    },
                    () =>
                    {
                        return this.head = newItem;
                    });
                newItem.Prev = maybeItem.Value.Prev;
                maybeItem.Value.Prev = newItem;
            }

            count = count + 1;
        }


        /// <summary>
        /// remove item as specified 0 based index
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            var maybeItem = TryGetItem(GetIndexOrArgException(index, count));

            var prev = 
                maybeItem.HasValue ?  
                    maybeItem.Value.Prev : 
                    last.Match(
                        x => x.Prev,
                        () => Empty.Value);
            var next =
                maybeItem.HasValue ?
                    maybeItem.Value.Next :
                    Empty.Value;

            prev.Match(
                prev2 => prev2.Next = next,
                () => head = next);
            next.Match(
                n2 => n2.Prev = prev,
                () => last = prev);

            count = count - 1;
        }

        /// <summary>
        /// remove all elements
        /// </summary>
        public void Clear()
        {
            this.head = Empty.Value;
            this.last = this.head;
            this.count = 0;
        }

        /// <summary>
        /// determines whether item is in list
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item) => this.Find(item).Item1.HasValue;

        /// <summary>
        /// remove 1st specified item from list, and return true, or false if not found
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            var (maybeItem, _) = Find(item);

            if (maybeItem.HasValue)
            {
                var prev =
                    maybeItem.HasValue ?
                        maybeItem.Value.Prev :
                        last.Match(
                            x => x.Prev,
                            () => Empty.Value);
                var next =
                    maybeItem.HasValue ?
                        maybeItem.Value.Next :
                        Empty.Value;

                prev.Match(
                    prev2 => prev2.Next = next,
                    () => head = next);
                next.Match(
                    n2 => n2.Prev = prev,
                    () => last = prev);

                count = count - 1;
            }

            return maybeItem.HasValue;
        }

        /// <summary>
        /// return enumerator that iterates through list
        /// </summary>
        /// <returns></returns>
        // cheating? see instructions.txt
        public IEnumerator<T> GetEnumerator() => this.AsEnumerable().GetEnumerator();

        /// <summary>
        /// return an IEnumerable (this is not normally what AsEnumerable does)
        /// </summary>
        /// <returns></returns>
        // cheating? see instructions.txt
#warning instructions tell us to NOT implement IEnumerable, BUT standard implementation of this usually casts   
        public IEnumerable<T> AsEnumerable() => AListItemUnion.ToEnumerable(head);
    }
}
