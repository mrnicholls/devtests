Hello,

so this was in many ways an interesting test, though in parts, I suspect deliberately, a bit frustrating...the requirements, 
I suspect are deliberately unclear, inconsistent and possibly inaccurate....I did for insert/remove 
laboriously implement while loops, then "Scan"s using continuations, then decided to refactor it, to
something more in line with what I'd actually do, rather than be paranoid about what the requirements were.

The "do not create an IEnumerator<T> derived enumerator class for GetEnumerator()", clause is problematic, because fundamentally 
IEnumerable is being used to create "Scan", which underpins most things...I'm still not sure whether "derived" means 
"derived class" as in inheritance, or "derived from", as in, is used in some way to constuct.

All the methods in question now sit on top of Find, that sits on top of Scan, that does use IEnumerable, deferring the execution 
of each iteration until its actually needed.

"AsEnumerable" is a similar case, where a programmer would normally expect that to be effectively a cast, but that contradicts 
the other instruction to not derive from IEnumerable<>.

The code is effectively all written in a tail recursive manner using while loops, with an explicit accumulator.
Default operators are used, instead of changing type constraints.
I havent changed any public interface, though some things I personally don't especially like (despite being those used by 
microsoft) I try to make methods "total". The code for SingleList and DoubleList is quite imperative, DoubleList is pretty
much inherently imperative (unless we resort to "tying the knot").

The Dictionary is partially pure, with the underlying data structure pure, except for use via a setter.

The code was not implemented using TDD, I'm more DDT myself, seeing tests as a good way to automatically regression test
rather than a mechanism to slavishly serve when writing new code (I quite like a REPL process when writing F#, but I havent
adopted it in C#). I did relearn FsCheck as this seemed ideal for this sort of thing.

I personally don't normally adopt the precondition => throw custom exception pattern, exceptions for me are edge cases (in a total
world, non existent), and adopting the said pattern is a significant cost for little gain, though I did adopt that for 
the use of indexes.

I have had some looks at your language extensions, so I know there are somethings you'd probably want done differently (e.g. I 
routinely model discriminated unions with "Match" functions).

The code contains unit tests, and fscheck tests, that are intended to be run in fsi.
